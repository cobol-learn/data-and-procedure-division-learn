       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA4.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 STUDENT-REC-DATA PIC X(44) VALUE "1205624WILLIAM FITZPATRICK 
      -    "19751021LM051385".

       01  STUDENT-REC.
        05   STUDENT-ID  PIC   9(7).
        05   STUDENT-NAME.
         10   FORNAME  PIC X(8).
         10   SURNAME  .
          15  F-SURNAME PIC X.
          15  FILLER PIC X(12).
        05   DATE-OF-BIRTH.
         08   YOB PIC  9999.
         08   MOB-DOB.
         10   MOB PIC 99.
         10   DOB   PIC   99.
        05   COURSE-ID  PIC   X(5).
        05   GPA  PIC   9V99.

       PROCEDURE DIVISION .
       Begin.
           DISPLAY STUDENT-REC-DATA.
           MOVE  STUDENT-REC-DATA TO STUDENT-REC .
           DISPLAY STUDENT-REC .
           DISPLAY STUDENT-ID.
      *    DISPLAY STUDENT-NAME.
           DISPLAY FORNAME .
           DISPLAY SURNAME .
           DISPLAY DATE-OF-BIRTH.
           DISPLAY COURSE-ID.
           DISPLAY GPA.
           DISPLAY DOB "/" MOB "/" YOB.
           DISPLAY F-SURNAME "." FORNAME.
           DISPLAY MOB-DOB .
           DISPLAY "MOB" MOB "DOB" DOB.

