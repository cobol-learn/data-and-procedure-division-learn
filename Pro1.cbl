       IDENTIFICATION DIVISION.
       PROGRAM-ID. PRO1.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 NUM1 PIC 99.
       01 NUM2 PIC 99.
       01 NUM3 PIC 99.
       01 NUM4 PIC 99.
       01 PROBLEM-STR PIC X(99).

       PROCEDURE DIVISION .
           PERFORM PROBLEM1 
           PERFORM PROBLEM2
           PERFORM PROBLEM3 
           PERFORM PROBLEM4
           PERFORM PROBLEM5
           PERFORM PROBLEM6
           PERFORM PROBLEM7 
           PERFORM PROBLEM8 
           PERFORM PROBLEM9 
           PERFORM PROBLEM10
           PERFORM PROBLEM11
           PERFORM PROBLEM12
           GOBACK.
      
      *PARAGRAPH
       PROBLEM1.
           MOVE "PROBLEM1: ADD NUM1 TO NUM2" TO PROBLEM-STR 
           MOVE 25 TO NUM1
           MOVE 30 TO NUM2
           MOVE 0 TO NUM3
           MOVE 0 TO NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           ADD NUM1 TO NUM2
           PERFORM DISPLAY-AFTER 
           EXIT.
       
      *PARAGRAPH
       PROBLEM2.
           MOVE "PROBLEM2: ADD NUM1 , NUM2 TO NUM3 , NUM4" TO 
                 PROBLEM-STR 
      *    NUM 3 = NUM3 + NUM 2+ NUM1 05+04+13 
      *    NUM 4 = NUM4 + NUM 2 +NUM1 12+04+13
           PERFORM HEADER .
           MOVE 13 TO NUM1 
           MOVE 04 TO NUM2
           MOVE 05 TO NUM3
           MOVE 12 TO NUM4
           PERFORM DISPLAY-BEFORE 
           ADD NUM1 , NUM2 TO NUM3 , NUM4
           PERFORM  DISPLAY-AFTER
           EXIT .

      *PARAGRAPH
       PROBLEM3.
           MOVE "PROBLEM3: ADD NUM1 NUM2 NUM3 GIVING NUM4" 
                 TO PROBLEM-STR .
      *    NUM 4 = NUM1 + NUM2 + NUM3
           MOVE 04 TO NUM1
           MOVE 03 TO NUM2
           MOVE 02 TO NUM3
           MOVE 01 TO NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           ADD NUM1 , NUM2 ,  NUM3 GIVING NUM4
           PERFORM DISPLAY-AFTER 
           EXIT.
      

            
       PROBLEM4.
           MOVE "PROBLEM4: SUBTRACT NUM1 FROM NUM2 GIVING NUM3" 
                 TO PROBLEM-STR .
      *    NUM3 = NUM2 - NUM1
           MOVE 04 TO NUM1
           MOVE 10 TO NUM2
           MOVE 55 TO NUM3
           MOVE 0  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           SUBTRACT NUM1 FROM NUM2 GIVING NUM3
           PERFORM DISPLAY-AFTER 
           EXIT.
       

       PROBLEM5.
           MOVE "PROBLEM5: SUBTRACT NUM1 , NUM2 FROM NUM3" 
                 TO PROBLEM-STR .
      *    NUM3 = NUM3 - NUM2 - NUM1
           MOVE 05 TO NUM1
           MOVE 10 TO NUM2
           MOVE 55 TO NUM3
           MOVE 0  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           SUBTRACT NUM1 , NUM2 FROM NUM3
           PERFORM DISPLAY-AFTER 
           EXIT.


       PROBLEM6.
           MOVE "PROBLEM5: SUBTRACT NUM1 , NUM2 FROM NUM 3 GIVING FROM 
      -         "NUM4" TO PROBLEM-STR .
      *    NUM4 = NUM3 - NUM2 - NUM1
           MOVE 05 TO NUM1
           MOVE 10 TO NUM2
           MOVE 55 TO NUM3
           MOVE 20  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           SUBTRACT NUM1 , NUM2 FROM NUM3 GIVING NUM4
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLEM7.
           MOVE "PROBLEM7: MULTIPLY NUM1 BY NUM2" TO PROBLEM-STR .
      *    NUM2 = NUM2 *  NUM1
           MOVE 05 TO NUM1
           MOVE 10 TO NUM2
           MOVE 55 TO NUM3
           MOVE 20  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           MULTIPLY NUM1 BY NUM2
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLEM8.
           MOVE "PROBLEM8: MULTIPLY NUM1 BY NUM2 GIVING NUM3" 
                 TO PROBLEM-STR .
      *    NUM3 = NUM2 *  NUM1
           MOVE 10 TO NUM1
           MOVE 5 TO NUM2
           MOVE 33 TO NUM3
           MOVE 00  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           MULTIPLY NUM1 BY NUM2 GIVING NUM3
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLEM9.
           MOVE "PROBLEM9: MULTIPLY NUM1 BY NUM2 GIVING NUM3" 
                 TO PROBLEM-STR .
      *    NUM2 = NUM2 / NUM1
           MOVE 05 TO NUM1
           MOVE 64 TO NUM2
           MOVE 00 TO NUM3
           MOVE 00  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           DIVIDE NUM1 INTO NUM2
           PERFORM DISPLAY-AFTER 
           EXIT.


       PROBLEM10.
           MOVE "PROBLEM10:  DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER 
      -         "NUM4" TO PROBLEM-STR .
      *    NUM2 = NUM2 / NUM1
           MOVE 05 TO NUM1
           MOVE 64 TO NUM2
           MOVE 24 TO NUM3
           MOVE 88  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4
           PERFORM DISPLAY-AFTER 
           EXIT.

       PROBLEM11.
           MOVE "PROBLEM11:  NUM1 = 5+10 *30/2" TO PROBLEM-STR .
           MOVE 25 TO NUM1
           MOVE 0 TO NUM2
           MOVE 0 TO NUM3
           MOVE 0  TO  NUM4
           PERFORM  HEADER 
           PERFORM DISPLAY-BEFORE 
           COMPUTE NUM1 = 5 + ((10*30)/2)
           PERFORM DISPLAY-AFTER 
           EXIT.



       PROBLEM12.
           DISPLAY "PLEASE INPUT FIRST NUM: " WITH NO ADVANCING 
           ACCEPT NUM1 
           DISPLAY "PLEASE INPUT SECOND NUM: " WITH NO ADVANCING 
           ACCEPT NUM2 
           COMPUTE NUM3 = NUM2 + NUM1 ON SIZE ERROR DISPLAY "ERROR .."
           END-COMPUTE.
           PERFORM DISPLAY-AFTER
           EXIT.



       
       HEADER.
           DISPLAY "*****************************************"
           DISPLAY PROBLEM-STR.
           DISPLAY "        NUM1 NUM2 NUM3 NUM4"

           EXIT.
       
       DISPLAY-BEFORE.
           DISPLAY "BEFORE: "NUM1 "   " NUM2 "   " NUM3 "   " NUM4.
           EXIT.
       
       DISPLAY-AFTER.
           DISPLAY "AFTER:  "NUM1 "   " NUM2 "   " NUM3 "   " NUM4
           EXIT.
           