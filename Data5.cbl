       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA5.
       AUTHOR. PAKAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 GRADE-DATA PIC X(90) VALUE "39030261WORAWIT         886345593B
      -    " 886352593D+886342193B+886478593C 886481592C+886491591A ".
       01 GRADE REDEFINES GRADE-DATA .
        03 STU-ID PIC 9(8).		
        03 STU-NAME PIC X(16).
        03 SUB1.
         05 SUB-CODE1 PIC 9(8).
         05 SUB-UNIT1 PIC 9.
         05 SUB-GRADE1 PIC X(2). 
        03 SUB2.
         05 SUB-CODE2 PIC 9(8).
         05 SUB-UNIT2 PIC 9.
         05 SUB-GRADE2 PIC X(2).
        03 SUB3.
         05 SUB-CODE3 PIC 9(8).
         05 SUB-UNIT3 PIC 9.
         05 SUB-GRADE3 PIC X(2). 
        03 SUB4.
         05 SUB-CODE4 PIC 9(8).
         05 SUB-UNIT4 PIC 9.
         05 SUB-GRADE4 PIC X(2). 
        03 SUB5.
         05 SUB-CODE5 PIC 9(8).
         05 SUB-UNIT5 PIC 9.
         05 SUB-GRADE5 PIC X(2). 
        03 SUB6.
         05 SUB-CODE6 PIC 9(8).
         05 SUB-UNIT6 PIC 9.
         05 SUB-GRADE6 PIC X(2). 
       
       66  STUDENT-ID RENAMES STU-ID .
       66  STUDENT-INFO RENAMES STU-ID THRU STU-NAME .
       01  STUDENT-NEW-CODE REDEFINES GRADE-DATA  .
           05 STU-NEW-YEAR PIC 9(2).
           05 FILLER PIC X(6).
           05 SHORT-NAME PIC X(3).


"

       PROCEDURE DIVISION .
       Begin.
           MOVE GRADE-DATA TO GRADE.
      *    /////
           DISPLAY "SUBJECT 1 :" SUB1.
      *    /////
           DISPLAY "SUBJECT 2"
           DISPLAY "CODE2 :" SUB-CODE2 .
           DISPLAY "UNIT2 :" SUB-UNIT2 .
           DISPLAY "GRADE2: " SUB-GRADE2 .
      *    /////
           DISPLAY "SUBJECT 3"
           DISPLAY "CODE3 :" SUB-CODE3 .
           DISPLAY "UNIT3 :" SUB-UNIT3 .
           DISPLAY "GRADE3: " SUB-GRADE3 .
      *    /////
           DISPLAY "SUBJECT 4"
           DISPLAY "CODE4 :" SUB-CODE4 .
           DISPLAY "UNIT4 :" SUB-UNIT4 .
           DISPLAY "GRADE4: " SUB-GRADE4 .
      *    /////
           DISPLAY "SUBJECT 5"
           DISPLAY "CODE5 :" SUB-CODE5 .
           DISPLAY "UNIT5 :" SUB-UNIT5 .
           DISPLAY "GRADE5: " SUB-GRADE5 .
      *    /////
           DISPLAY "SUBJECT 6"
           DISPLAY "CODE6 :" SUB-CODE6 .
           DISPLAY "UNIT6 :" SUB-UNIT6 .
           DISPLAY "GRADE6: " SUB-GRADE6 .
      *    //////
           DISPLAY STUDENT-ID .
           DISPLAY STUDENT-INFO .
      *    ////
           DISPLAY STU-NEW-YEAR SHORT-NAME.