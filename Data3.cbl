       IDENTIFICATION DIVISION.
       PROGRAM-ID. DATA3.
       AUTHOR. PAKAWAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION. 
       01  SURNAME  PIC   X(8)  VALUE "COUGHLAN".
       01  SALE-PRICE  PIC   9(4)V99.   
       01  NUM-OF-EMP  PIC   999V99.
       01  SALARY   PIC   9999V99.
       01  COUNTY-NAME PIC   X(9). 
       PROCEDURE DIVISION .
       Begin.
           DISPLAY "SURNAME :" SURNAME .
           MOVE "SMITH" TO SURNAME.
           DISPLAY "SURNAME :" SURNAME .
           MOVE "FITZWILLIAM" TO SURNAME.
           DISPLAY "SURNAME :" SURNAME .
      *    /////////////////////////////
           DISPLAY "1 " SALE-PRICE. 
           MOVE ZEROS TO SALE-PRICE .
           DISPLAY "2 " SALE-PRICE . 
           MOVE 25.5 TO SALE-PRICE.
           DISPLAY "3 " SALE-PRICE .
           MOVE 7.553 TO SALE-PRICE. 
           DISPLAY "4 " SALE-PRICE .
           MOVE 93425.158 TO SALE-PRICE.
           DISPLAY "5 " SALE-PRICE .
           MOVE 128 TO SALE-PRICE.
           DISPLAY "6 " SALE-PRICE .
      *    ///////////////////////////////
           DISPLAY NUM-OF-EMP .
           MOVE  2.4   TO NUM-OF-EMP .
           DISPLAY "1 " NUM-OF-EMP .
           MOVE  6745   TO NUM-OF-EMP .
           DISPLAY "2 " NUM-OF-EMP .
      *01  SALARY   PIC   9999V99.
           MOVE NUM-OF-EMP   TO SALARY .
      *    0745.00
           DISPLAY SALARY .
      *01  COUNTY-NAME PIC   X(9).  <-- alphanumeric
           MOVE "GALWAY" TO COUNTY-NAME .
      *    GALWAY***
           DISPLAY COUNTY-NAME .
           MOVE ALL "BEST"  TO COUNTY-NAME .
      *    *********
           DISPLAY COUNTY-NAME .
           